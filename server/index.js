const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();

const PORT = process.env.PORT || 3000;

app.use(cors());
app.use(bodyParser.json());

app.get("/users", (req, res) => {
    res.json(require("./data/users.json"));
});

app.post("/login", (req, res) => {

    console.log("received data: \n" + JSON.stringify(req.body, null, 4));

    res.json({
        token: Math.random().toString(16).slice(2)
    });
});

app.listen(PORT, () => console.log(`server is listening on port ${PORT}`));