import _ from 'lodash'

export default {
    user(state, getters) {
        return id => {
            let index = _.findIndex(state.users, ["id", id]);

            return state.users[index];
        }
    },
    females(state, getters) {
        return state.users.filter(user => user.gender === 'female');
    },
    males(state, getters) {
        return state.users.filter(user => user.gender === 'male');
    },
}