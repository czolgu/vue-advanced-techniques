import Vue from 'vue';
import Vuex from 'vuex';
import _ from 'lodash';

import mutations from './mutations'
import actions from './actions'
import getters from './getters'

Vue.use(Vuex);

export default new Vuex.Store({
    state: { // this state will be shared by all the instances, all the components of Vue app
        users: [],
    },
    getters,
    mutations,
    actions,
})