export default {
    updateUserAge(context, payload) {
        context.commit('updateUserAge', payload);
    },
    updateUserName(context, payload) {
        context.commit('updateUserName', payload);
    },
    removeUser(context, payload) { // context.state and context.commit available in context, data in payload
        setTimeout(() => {
            context.commit('removeUser', payload);
        }, 3500);
    }
}