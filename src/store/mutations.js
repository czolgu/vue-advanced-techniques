export default {
    updateUserAge(state, payload) {
        const index = _.findIndex(state.users, ['id', payload.userId]);
        state.users[index].age = payload.newValue;
    },
    updateUserName(state, payload) {
        const index = _.findIndex(state.users, ['id', payload.userId]);
        state.users[index].name = payload.newValue;
    },
    updateUserGender(state, payload) {
        const index = _.findIndex(state.users, ['id', payload.userId]);
        state.users[index].gender = payload.newValue;
    },
    removeUser(state, payload) {
        const { userId } = payload;
        const index = _.findIndex(state.users, ['id', userId]);
        state.users.splice(index, 1);   // splice method is also overwritten by vue, and is reactive, so you dont need to
                                        // overwrite current 'users' array after you splice the user you want to delete.
    },
}