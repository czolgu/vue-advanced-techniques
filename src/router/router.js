import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes'

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history', // this will remove the '#' character before routes
    routes,
    linkActiveClass: 'show-me-active',
    scrollBehavior(to, from, savedPosition) {
        return {x: 0, y: 0}
    }
});

router.beforeEach((to, from, next) => {
    if (to.meta.needsAuthorization === false || to.meta.needsAuthorization === undefined) { // needsAuthorization is the name of a meta of the route.
                                                // It says if we need to check if the user is logged in
        return next();
    }
    // console.log(from);
    // 'this' reference is not available here! 
    // this method will get called when some route will try to render this component, before its rendered
    if (window.localStorage.getItem('token') ) {
        next();
    } else {
        next({
            name: 'login'
        });
    }
});

export default router;