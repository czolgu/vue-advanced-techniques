import Home from '../components/Home'
import UserList from '../components/UserList'
// import UserDetails from '../components/UserDetails'
// import UserEdit from '../components/UserEdit'
import HomeNavbar from '../components/HomeNavbar'
import UsersNavbar from '../components/UsersNavbar'
import UserLogin from '../components/UserLogin'

const UserDetails = () => import('../components/UserEdit');
const UserEdit = () => import('../components/UserEdit'); // method, that will load 'UserEdit' file
                                                         // this method can be passed to 'component' or 'components' array of each route.
                                                         // Vue will call this method and only after the file is loaded it will load the component.

export default [
    {   
        path: '/', 
        redirect: {
            name: 'users',
        }
    },
    {
        path: '/home', 
        components: {
            default: Home,
            header: HomeNavbar,
        }, 
        name: 'home'
    },
    {
        path: '/users', 
        components: {
            default: UserList,
            header: UsersNavbar,
        }, 
        name: 'users'
    },
    {
        path: '/user/:id', 
        component: UserDetails,
        name: 'user', 
        props: true,    // means, that the wildcard: 'id' in this route will be passed to the component as 
                        // a prop'
        meta: {
            needsAuthorization: true,
        },
        children: [{
            path: 'edit', 
            component: UserEdit, 
            name: 'user-edit',
            props: true,
            meta: { // data, that guards can access
                needsAuthorization: true,
            },
        }
    ]},
    {
        path: '/login',
        component: UserLogin,
        name: 'login',
    },
]